Evaluation Criteria
====================

General Correctness
------------------
* Does filtering work?
    * Age
    * Gender
* Is the first name + last name displayed as the full user name?
* Is the displayed age correct?
* Do search results include the inclusive boundaries of the age range? I.e. for 18-35, are 18 and 35 included?
* Is the correct # of candidates displayed at all times? Both before and after filtering?
* Does the reset button display all initial search results? Does it clear the input fields?

Code Quality
--------------
* How readable is the code?
* Is the code well-factored?  Do components live in separate files, or is everything in a single file?

Web API Call
------------
* Does the API call work? Did the candidate use fetch? $.ajax? XMLHttpRequest? *SYNCHRONOUS XMLHttpRequest*?
* How did the candidate handle the API call? Is he using promises or callbacks? 
* How did the candidate handle fetching 10 records? 
    * Does he make 10 individual API calls? If so, is he firing them all off simultaneously, or waiting for one at a time? 
    * Did he read the API documentation and realize he can fetch 10 with a single call?
* What does the UI display before it gets the results? A spinner? Nothing?


Age
---------------
* How did the candidate handle transforming the resulting date of birth into age?
    * Did he use the built-in Date methods?

Catchphrase
---------------
* Did the user understand how to import an npm module and use the functionality?

HTML
---------------
* When re-writing the search results, did the candidate correct any of the non-semantic boost tags?
* Did the candidate use any responsive markup?

Display updates
--------------
* Are there any memory leaks when refreshing the results? (May need to introduce event handlers on the results)

Possible additions
====================

Contact Info on click
-----------------
* Each match should display a details window when clicked, that contains extra details from the API, such as their e-mail, their phone #, and their cell #
* This would force the user to use an event-handler, which can create memory leaks in backbone or react.

Favorite Color
----------------
* Could ask the user to create a function to generate a random favorite color for each result.
* This would demonstrate an ability to create very basic pure javascript functionality

Error Handling
-----------------
* We could ask users to create an error-handling solution if the external API does not work

Unit Testing
-----------------
* We could ask for some limited amount of unit testing. Perhaps a small segment of logic. We could also ask more senior candidates to stub out API calls

